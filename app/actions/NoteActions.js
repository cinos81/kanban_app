/**
 * Created by cinos81 on 16. 1. 28..
 */
import alt from '../libs/alt';
export default alt.generateActions('create', 'update', 'delete');