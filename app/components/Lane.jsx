import AltContainer from 'alt-container';
import React from 'react';
import Notes from './Notes';
import NoteActions from '../actions/NoteActions';
import NoteStore from '../stores/NoteStore';

export default class Lane extends React.Component {
	render(){
		const {lane, ...props} = this.props;

		return (
			<div {...props}>
				<div className="lane-header">
					<div className="lane-add-note">
						<button onClick={this.addNote}>+</button>
					</div>
					<div className="lane-name">{lane.name}</div>
				</div>
				<AltContainer stores={[NoteStore]}
					inject={{
						notes: () => NoteStore.getState().notes || []
					}}
					>
					<Notes onEdit={this.editNote} onDelete={this.deleteNote}></Notes>
				</AltContainer>
			</div>
		);
	}

	addNote(){
		NoteActions.create({task:'new task'});
	}

	editNote(id, task){
		NoteActions.update(id, task);
	}

	deleteNote(id){
		NoteActions.delete(id);
	}
}