/**
 * Created by cinos81 on 16. 1. 29..
 */
import React from 'react';
import Lane from './Lane.jsx';

export default ({lanes}) => {
	return (
		<div className="lanes">
			{lanes.map(lane =>
				<Lane className="lane" key={lane.id} lane={lane}></Lane>
			)}
		</div>
	);
}