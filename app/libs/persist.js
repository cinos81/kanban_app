/**
 * Created by cinos81 on 16. 1. 29..
 */
import makeFinalStore from 'alt-utils/lib/makeFinalStore';

export default function(alt, storage, storeName){
	const finalStore = makeFinalStore(alt);
	try{
		alt.bootstrap(storage.get(storeName));
	}
	catch(e){
		console.error('데이터 부트스트래핑 실패', e);
	}

	finalStore.listen(()=>{
		if(!storage.get('debug')){
			storage.set(storeName, alt.takeSnapshot());
		}
	});
};