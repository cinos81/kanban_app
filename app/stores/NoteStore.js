/**
 * Created by cinos81 on 16. 1. 28..
 */
import uuid from 'node-uuid';
import alt from '../libs/alt';
import NoteActions from '../actions/NoteActions';

//Note의 State 저장소
//CUD 후에 변화가 감지되면 View의 리스너에 상태를 전달한다.
class NoteStore {
	constructor(){
		this.bindActions(NoteActions);

		this.notes = [];
	}
	//새로운 노트에 아이디를 부여하고, 그 노트를 NoteStore인스턴스 안에 보관한다.
	//새로운 노트는 Flux에서의 state가 된다.
	create(note){
		const notes = this.notes;
		note.id = uuid.v4();
		this.setState({
			notes:notes.concat(note)
		});
	}

	update(updateNote){
		const notes = this.notes.map(note => {
			if(note.id === updateNote.id){
				//_.extend와 비슷한 기능을 한다.
				return Object.assign({}, note, updateNote);
			}
			return note;
		});
		//아래의 코드는 `this.setState({notes:notes});` 와 동일하다고 한다.
		//이 기능의 이름은 property shorthand라고 한다고
		this.setState({notes});
	}
	delete(id){
		this.setState({
			//filter메소드는 배열 요소를 삭제하지 않고 배제한 다음 리턴한다.
			//native 기능이니 빠르겠지?
			notes: this.notes.filter(note => note.id !== id)
		})
	}
}

export default alt.createStore(NoteStore, 'NoteStore');